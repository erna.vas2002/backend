import { GroupModel } from '../models/groupModel'

export const getGroupServise = async () => {
    try {
        return await GroupModel.find().populate('categoryId').exec()
    } catch (error) {
        return error
    }
}

export const getGroupByIdServise = async (id: string) => {
    try {
        return await GroupModel.findById(id)
    } catch (error) {
        return error
    }
}

export const createGroupServise = async (req: any): Promise<any> => {
    try {
        
        const result = await GroupModel.create({
            name: req.body.name,
            groupImage: `http://localhost:4000/static/${req.file.filename}`,
            categoryId: req.body.categoryId
        })
        
        return result
    } catch (error) {
        return error
    }
}

export const deleteGroupServise = async (id: Object) => {
    try {

        await GroupModel.findOneAndDelete(id)
        return id
    } catch (error) {
        return error
    }
}

