import { ImagesModel } from '../models/imagesModel'

export const createImages = async (req: any): Promise<any> => {
    try {

        const images = req.body.images

        const result = await ImagesModel.create({
            image: `http://localhost:4000/static/${req.file.filename}`,
            productId: req.body.productId
        })

        return result
    } catch (error) {
        return error
    }
}