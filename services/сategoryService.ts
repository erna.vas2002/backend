import { CategoryModel } from '../models/categoryModel'

interface сategory {
    title: string
}

export const CategoryService = {
    getCategoryInfo: async () => {
        try {
            return await CategoryModel.find()
        } catch (error) {
            return error
        }
    },

    createCategoryData: async (params: сategory): Promise<any> => {
        try {
            
            const result = await CategoryModel.create({
                title: params.title
            })

            return result
        } catch (error) {
            return error
        }
    },

    deleteCategory: async (id: any) => {
        try {

            const result = await CategoryModel.findOneAndDelete(id)

        } catch (error) {
            return error
        }
    },
    
}