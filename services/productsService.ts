import { ProductsModel } from '../models/productsModel'

export const getProducts = async () => {
    try {
        return await ProductsModel.find().populate("categoryId").populate("groupId")

    } catch (error) {
        return error
    }
}

export const getProductById = async (id: string) => {
    try {
        return await ProductsModel.findById(id)
    } catch(error) {
        return error
    }
}

export const getProductByIdGroup = async (groupId: string ) => {
    try {
        const product = await ProductsModel.find({ groupId: groupId })
        console.log(product)
        return product;
    } catch (error) {
        console.log("error")
        return error
    }
}

export const createProduct = async (req: any): Promise<any> => {
    try {
        
        const result = await ProductsModel.create({
            name: req.body.name,
            categoryId: req.body.categoryId,
            groupId: req.body.groupId,
            // productImage: `http://localhost:4000/static/${req.file.filename}`,
            price: req.body.price,
            discount: req.body.discount,
            rate: req.body.rate
        })
        
        return result
        
    } catch (error) {
        return error
    }
}

export const deleteProduct = async (id: Object) => {
    try {

        await ProductsModel.findOneAndDelete(id)
        return id
    } catch (error) {
        return error
    }
}