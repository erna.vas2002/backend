import Joi from 'joi'

import { Request, Response } from 'express'

export const validate = (schema: Joi.ObjectSchema<any>) => {
    return (req: Request, res: Response, next: Function) => {
       
        const { error } = schema.validate(req.body);
        if (error) {
            return res.status(400).json({ message: error.details[0].message });
        }
        next();
    };
};