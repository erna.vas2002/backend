import { getGroupServise, getGroupByIdServise, deleteGroupServise, createGroupServise } from '../services/groupService'
import { Request, Response } from 'express'

// interface ProductGroupType extends Request {
//     name: string,
//     categoryId: ObjectId,
//     file: {
//         filename: string
//     }
// }

export const GoodsController = {

    getGroup: async (req: Request, res: Response): Promise<void> => {
        try {
            const groupsList = await getGroupServise()

            res.status(201).json(groupsList)
        } catch (error) {
            res.status(500).json('Не удалось получить товары, повторите попытку!')
        }
    },

    getGroupById: async (req: Request, res:  Response): Promise<void> => {
        try {
            const groupId = await getGroupByIdServise(req.params.id)

            res.status(201).json(groupId)
        } catch (error) {
            res.status(500).json('Не удалось получить список товаров, повторите попытку!')
        }
    },

    deleteGroup: async (req: Request, res: Response): Promise<void> => {
        try {
            const prodGroupId = await deleteGroupServise(req.params.id)

            res.status(201).json({ id: prodGroupId })
        } catch (error) {
            res.status(500).json({ message: 'Не удалось удалить категорию товаров!' })
        }
    },

    createGroup: async (req: Request, res: Response): Promise<void> => {
        try {
            
            const productGroup = await createGroupServise(req)

            res.status(201).json(productGroup)
        } catch (error) {
            res.status(500).json({ message: 'Не удалось создать категории товаров!' })
        }
    }
}