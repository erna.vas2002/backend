import { CategoryService } from '../services/сategoryService'
import { Request, Response } from 'express';

export const CategoryController = {

    getCategories: async (req: Request, res: Response) => {
        try {
            const allCategories = await CategoryService.getCategoryInfo()

            res.status(200).json(allCategories)
        } catch (error) {

            res.status(500).json({ message: "Interval Server Error" })
        }
    },

    createCategories: async (req: Request, res: Response): Promise<void> => {
        try {

            const newCategory = await CategoryService.createCategoryData(req.body)

            res.status(201).json(newCategory)

        } catch (error) {

            res.status(500).json({ message: "Не удалось создать категорию" })
        }
    },

    deletedCategories: async (req: Request, res: Response, next: Function) => {
        try {
            
            const idCategory = req.params.id

            await CategoryService.deleteCategory({_id: idCategory})
            
            res.status(201).json({id: idCategory})
            
        } catch (error) {
            res.status(500).json({ message: "Не удалось удалить категорию" })
            
        }
    }
}

