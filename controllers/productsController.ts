import { getProducts, getProductById, createProduct, deleteProduct, getProductByIdGroup } from '../services/productsService';
import { Request, Response } from 'express';

export const productsController = {

    getProductsController: async (req: Request, res: Response) => {
        try {
            const product = await getProducts()

            res.status(201).json(product)
        } catch (error) {
            res.status(500).json('Не удалось получить товары, повторите попытку!')
        }
    },

    getProductByIdController: async (req: Request, res: Response) => {
        try {
            
            const productId = await getProductById(req.params.id)

            return res.status(201).json(productId)
        } catch (error) {
            res.status(500).json('Не удалось получить товары, повторите попытку!')
        }
    },

    getProductByIdGroupController: async (req: Request, res: Response) => {
        try {
            const groupId = req.params.id;
            const products = await getProductByIdGroup(groupId)
            return res.status(200).json(products);
        } catch (error) {
            res.status(500).json('Не удалось получить товары по группам, повторите попытку!')
        }
    },

    deleteProductController: async (req: Request, res: Response) => {
        const productId = req.params.id
        try {
            const productId = await deleteProduct(req.params.id)

            return res.status(201).json({id: productId})
        } catch (error: unknown) {
            if (error instanceof Error) {
                return res.status(404).json({ message: `Товар с id - ${productId} не найден` })
            } else {
                return res.status(500).json({ message: 'Internal Server Error' });
        }
    }
    },

    createProductController: async (req: Request, res: Response) => {
        try {

            const newProduct = await createProduct(req) 

            res.status(201).json(newProduct)
        } catch (error: unknown) {
            if (error instanceof Error) {
                res.status(500).json({ message: 'Failed to create product' })
            }
        }
    }
}