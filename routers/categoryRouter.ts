import express from 'express'
import { CategoryController } from '../controllers/categoryController';
import { validate } from '../helpers/validate';
import { CategorySchema } from '../validation/categorySchema '

export const routerCategories = express.Router()

routerCategories.get('/', CategoryController.getCategories);
routerCategories.post('/', CategoryController.createCategories);
routerCategories.delete('/:id', CategoryController.deletedCategories);