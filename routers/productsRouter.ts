import express from 'express'
import { productsController } from '../controllers/productsController'
import multer, { StorageEngine } from 'multer'
import path from 'path'

export const routerProducts = express.Router()

const storage: StorageEngine = multer.diskStorage({
    destination: './assets',
    filename: (req, file, cb) => {
        cb(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname))
    }
})

const upload = multer({ storage })

routerProducts.get('/', productsController.getProductsController)
routerProducts.get('/product/:id', productsController.getProductByIdController)
routerProducts.get('/:id', productsController.getProductByIdGroupController)
routerProducts.post('/', upload.single('productImage'), productsController.createProductController)
routerProducts.delete('/:id', productsController.deleteProductController)