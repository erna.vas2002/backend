import express from 'express'
import { GoodsController } from '../controllers/groupController'
import multer, { StorageEngine } from 'multer'
import path from 'path'

export const routerGroup = express.Router()

const storage: StorageEngine = multer.diskStorage({
    destination: './assets',
    filename: (req, file, cb) => {
        cb(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname))
    }
})

const upload = multer({ storage })

routerGroup.get('/', GoodsController.getGroup)
routerGroup.get('/:id', GoodsController.getGroupById)
routerGroup.post('/', upload.single('groupImage'), GoodsController.createGroup)
routerGroup.delete('/:id', GoodsController.deleteGroup)