import Joi from 'joi'

export const CategorySchema = Joi.object({
    name: {
        type: Joi.string().required()
    },
    productsGroupId: {
        type: Joi.string().pattern(/^[0-9a-fA-F]{24}$/).required()
    },
    categoryId: {
        type: Joi.string().pattern(/^[0-9a-fA-F]{24}$/).required()
    },
})