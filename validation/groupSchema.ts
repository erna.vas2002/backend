import Joi from 'joi'

export const GroupSchema = Joi.object({
    name: Joi.string().required(),
    categoryId: Joi.string().pattern(/^[0-9a-fA-F]{24}$/).required()
})