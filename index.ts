import express from 'express'
import mongoose from 'mongoose'
const app = express()
const port = 4000
import { routerCategories } from './routers/categoryRouter'
import { routerGroup } from './routers/groupRouter'
import { routerProducts } from './routers/productsRouter'
import { validate } from './helpers/validate'
import { CategorySchema } from './validation/categorySchema '
import cors from 'cors'
import { ProductsModel } from './models/productsModel'
import { GroupModel } from './models/groupModel'


app.use(cors())
app.use(express.json())

app.use(express.urlencoded({ extended: true }));

app.use("/static", express.static(__dirname + "/assets"))

app.use('/api/category', routerCategories)
app.use('/api/group', routerGroup)
app.use('/api/products', routerProducts)


mongoose.connect('mongodb://localhost:27017')
    .then(() => {
        app.listen(port, () => {
            console.log(`Example app listening on port ${port}`)
        })
    })
    // .then(() => {
    //     change()
    // })

// const change = async () => {
//     try {
//         const products = await GroupModel.find()
//         for (const product of products) {
//             let updatedProductImage = product.groupImage


//             if (updatedProductImage.includes(':/4000')) {
//                 updatedProductImage = updatedProductImage.replace(':/4000', ':4000')
//             }

//             await GroupModel.findByIdAndUpdate(product._id, {
//                 groupImage: updatedProductImage
//             })
//         }
//         console.log('Products updated successfully')
//     } catch (error) {
//         console.error('Error updating products:', error)
//     }
// }

// change()