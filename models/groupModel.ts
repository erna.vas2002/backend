import mongoose, { CallbackError, Schema } from 'mongoose'
import { ProductsModel } from './productsModel'

interface GroupType extends Request {
    name: string
    groupImage: string
    categoryId: string
}

export const groupSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    groupImage: {
        type: String,
        required: true
    },
    categoryId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'CategoryModel',
        require: true
    }
})

groupSchema.post("findOneAndDelete", async function (doc, next) {
    if (doc) {
        console.log(doc, 'doc in group model')
        try {
            await ProductsModel.deleteMany({ GroupId: doc._id })
        } catch (error) {
            console.error("Ошибка при удалении связанных продуктов:", error)
            next(error as CallbackError)
            return
        }
    }
    next()
})

export const GroupModel = mongoose.model<GroupType>('GroupModel', groupSchema);