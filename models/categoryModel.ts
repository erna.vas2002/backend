import * as mongoose from 'mongoose'
import { Schema, Document, CallbackError } from 'mongoose'

interface CategoryType {
    title: string
}

export const CategorySchema = new Schema({
    title: { 
        type: String, 
        required: true 
    }
});

CategorySchema.post("findOneAndDelete", async function (doc, next) {
    if (doc) {
        console.log(doc)
        try {
            await mongoose.model('groupModel').deleteMany({ categoryId: doc._id })
            await mongoose.model('ProductModel').deleteMany({ categoryId: doc._id })
        } catch (error) {
            console.error("Ошибка при удалении связанных групп продуктов:", error)
            next(error as CallbackError)
            return
        }
    }
    next()
});

export const CategoryModel = mongoose.model<CategoryType>('CategoryModel', CategorySchema)