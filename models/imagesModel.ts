import mongoose, { Schema } from 'mongoose'

interface ImagesType {
    imageUrl: string
    productId: string
}

export const imagesSchema = new Schema({
    imageUrl: {
        type: String,
        required: true
    },
    productId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'ProductsModel',
        require: true
    }
})

export const ImagesModel = mongoose.model<ImagesType>('ProductsModel', imagesSchema)