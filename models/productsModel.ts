import mongoose, { Schema } from 'mongoose'
import { ObjectId } from 'mongodb'

interface ProductsType {
    name: string,
    productImage: [string],
    productsGroupId: ObjectId,
    categoryId: ObjectId,
    price: number,
    discount: number,
    rate: number,
}

export const productsSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    productImage: {
        type: [String],
        require: true,
    },
    groupId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'GroupModel',
        require: false,
    },
    categoryId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'CategoryModel',
        require: false,
    },
    price: {
        type: Number,
        required: true
    },
    discount: {
        type: Number,
        required: false
    },
    rate: {
        type: Number,
        required: true
    }
})

export const ProductsModel = mongoose.model<ProductsType>('ProductsModel', productsSchema)